Source: libfilezilla
Priority: optional
Maintainer: Adrien Cunin <adri2000@ubuntu.com>
Build-Depends: debhelper-compat (= 13),
               libcppunit-dev,
               pkg-config,
               nettle-dev (>= 3.1),
               libgnutls28-dev (>= 3.5.7)
Standards-Version: 4.5.0
Rules-Requires-Root: no
Section: libs
Homepage: https://lib.filezilla-project.org/
Vcs-Git: https://salsa.debian.org/debian/libfilezilla.git
Vcs-Browser: https://salsa.debian.org/debian/libfilezilla

Package: libfilezilla-dev
Section: libdevel
Architecture: any
Depends: libfilezilla0 (= ${binary:Version}), ${misc:Depends}
Description: build high-performing platform-independent programs (development)
 Free, open source C++ library, offering some basic functionality to build
 high-performing, platform-independent programs. Some of the highlights include:
 .
  - A typesafe, multi-threaded event system that's very simple to use yet
    extremely efficient
  - Timers for periodic events
  - A datetime class that not only tracks timestamp but also their accuracy,
    which simplifies dealing with timestamps originating from different sources
  - Simple process handling for spawning child processes with redirected I/O
 .
 This package contains the development files for the library.

Package: libfilezilla0
Architecture: any
Breaks: filezilla (<= 3.39.0-2)
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: build high-performing platform-independent programs (runtime lib)
 Free, open source C++ library, offering some basic functionality to build
 high-performing, platform-independent programs. Some of the highlights include:
 .
  - A typesafe, multi-threaded event system that's very simple to use yet
    extremely efficient
  - Timers for periodic events
  - A datetime class that not only tracks timestamp but also their accuracy,
    which simplifies dealing with timestamps originating from different sources
  - Simple process handling for spawning child processes with redirected I/O
 .
 This package contains the shared library.
