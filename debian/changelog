libfilezilla (0.24.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.24.1
  * Add 'debian/upstream/metadata'

 -- Phil Wyett <philip.wyett@kathenas.org>  Mon, 24 Aug 2020 11:20:46 +0100

libfilezilla (0.23.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.23.0
  * Add hardening
  * Use override_dh_auto_install and remove .la file(s)
  * Bump std-version to 4.5.0, no changes required
  * Bump debhelper-compat to 13
  * Add Rules-Requires-Root: no

 -- Phil Wyett <philip.wyett@kathenas.org>  Thu, 30 Jul 2020 21:03:46 +0100

libfilezilla (0.19.3-1) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * Import the new release on git

  [ Phil Wyett ]
  * Team upload
  * New upstream version 0.19.3

 -- Phil Wyett <philip.wyett@kathenas.org>  Mon, 23 Dec 2019 20:28:17 +0000

libfilezilla (0.18.2-4) unstable; urgency=medium

  * Team upload
  * Bump std-version to 4.4.1, no changes required
  * Switch to new debhelper-compat 12 notation
  * Break filezilla << 3.45.1-1 (Closes: #943433)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Fri, 25 Oct 2019 12:15:45 +0200

libfilezilla (0.18.2-3) unstable; urgency=medium

  * Add latomic to fix link failures on some architectures
    such as armel, mipsel, m68k, powerpc, sh4

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 23 Oct 2019 23:59:12 +0200

libfilezilla (0.18.2-2) unstable; urgency=medium

  * Break filezilla <= 3.39.0-2, to avoid crashes due to changes in symbol
    files (Closes: #941193)
    Don't use << of the next version, since 3.39.0-2 is already on stable, and
    as simple binNMU will make it work with the current libfilezilla.

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 23 Oct 2019 23:34:15 +0200

libfilezilla (0.18.2-1) unstable; urgency=medium

  * New upstream release
  * Added libgnutls28-dev build-dep
  * Updated Standards-Version to 4.4.0, no change needed
  * Updated dh compat to 12
  * debian/libfilezilla0.install: include translations

 -- Adrien Cunin <adri2000@ubuntu.com>  Mon, 23 Sep 2019 09:38:28 +0200

libfilezilla (0.15.1-1) unstable; urgency=medium

  * New upstream release
  * Build-dep on nettle-dev (>= 3.1)
  * debian/watch: use https

 -- Adrien Cunin <adri2000@ubuntu.com>  Mon, 07 Jan 2019 11:23:52 +0100

libfilezilla (0.12.3-1) unstable; urgency=medium

  * New upstream release
  * Updated Vcs-* URLs with Salsa ones

 -- Adrien Cunin <adri2000@ubuntu.com>  Fri, 15 Jun 2018 15:18:47 +0200

libfilezilla (0.11.0-1) unstable; urgency=medium

  * Team upload
  * Bump std-version to 4.1.1
  * New upstream version 0.11.0

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Sun, 29 Oct 2017 12:39:47 +0100

libfilezilla (0.10.0-1) unstable; urgency=medium

  * Team upload.
  * Update watch file
  * New upstream version 0.10.0
  * Add pkg-config from <rene@debian.org> (Closes: #862125)
  * Bump std-version to 4.0.0, no changes required
  * Bump autoreconf to 10
  * Update copyright years

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 11 Jul 2017 09:54:59 +0200

libfilezilla (0.9.0-1) unstable; urgency=medium

  * New upstream release

 -- Adrien Cunin <adri2000@ubuntu.com>  Tue, 17 Jan 2017 16:13:46 +0100

libfilezilla (0.7.0-1) unstable; urgency=medium

  * Team Upload
  * New upstream release

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Tue, 27 Sep 2016 10:56:33 +0200

libfilezilla (0.6.1-1) unstable; urgency=medium

  * New upstream release

 -- Adrien Cunin <adri2000@ubuntu.com>  Thu, 28 Jul 2016 11:44:04 +0200

libfilezilla (0.6.0-1) unstable; urgency=medium

  * New upstream release
  * Use dh_makeshlibs -V so that packages built against libfilezilla get a
    correct versioned dependency
  * Fixed libfilezilla0 description: it does not include the static library

 -- Adrien Cunin <adri2000@ubuntu.com>  Tue, 26 Jul 2016 15:16:05 +0200

libfilezilla (0.5.3-1) unstable; urgency=medium

  * New upstream release
  * Updated Standards-Version to 3.9.8, no change needed

 -- Adrien Cunin <adri2000@ubuntu.com>  Wed, 06 Jul 2016 16:07:41 +0200

libfilezilla (0.4.0.1-1) unstable; urgency=low

  * Initial release (Closes: #815763)

 -- Adrien Cunin <adri2000@ubuntu.com>  Wed, 17 Feb 2016 16:04:03 +0100
